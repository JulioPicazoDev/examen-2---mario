package com.softtek.academy.javaweb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.javaweb.beans.Person;

@Controller
public class PersonController {
	 @Autowired
	    Dao dao;//will inject dao from xml file
	    @RequestMapping(value = "/person", method = RequestMethod.GET)
		public ModelAndView student() {
			return new ModelAndView("person", "command", new Person());
		}
	    /*It displays a form to input data, here "command" is a reserved request attribute
	     *which is used to display object data into form
	     */
	  
	    /*It saves object into database. The @ModelAttribute puts request data
	     *  into model object. You need to mention RequestMethod.POST method
	     *  because default request is GET*/
	    @RequestMapping(value="/save",method = RequestMethod.POST)
	    public String save(@ModelAttribute("emp") Person emp){
	        dao.save(emp);
	        return "redirect:/viewperson";//will redirect to viewemp request mapping
	    }
	 
	 
	    /* It provides list of employees in model object */
	    @RequestMapping("/viewperson")
	    public String viewemp(Model m){
	        List<Person> list=dao.getPersons();
	        m.addAttribute("list",list);
	        return "viewperson";
	    }
	    /* It displays object data into form for the given id.
	     * The @PathVariable puts URL data into variable.*/
	
	    
}
